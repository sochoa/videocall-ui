# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:13.8.0-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY ./ /app/
RUN npm run build -- --output-path=./dist/videocall-ui-component --prod --aot

#Stage serve
FROM nginx:stable

COPY --from=builder /app/dist/videocall-ui-component/* /var/www/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d/default.conf.template

# Default configuration
ENV PORT 80
ENV SERVER_NAME _
ENV uri \$uri
ENV BASEHREF = '/videocall-ui-component-external/'

#DEFINE ENV VARIABLE BY DEFAULT
ENV BASE_ENDPOINT='https://k8s-dev.chattigo.com/feature-eqp1-576/bff-portal-admin'


CMD ["sh", "-c", "envsubst < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]



