import {CUSTOM_ELEMENTS_SCHEMA, NgModule, Injector} from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AngularDraggableModule } from 'angular2-draggable';
import { AppRoutingModule } from './routing/routing.component';
import { AppComponent } from './app.component';
import { BtnVideoComponent } from './components/btn-video/btn-video.component';
import { PublisherComponent } from './components/publisher/publisher.component';
import { SubscriberComponent } from './components/subscriber/subscriber.component';
import { VideoPublisherComponent } from './pages/video-publisher/video-publisher.component';
import { VideoSubscriberComponent } from './pages/video-subscriber/video-subscriber.component';
import {APP_BASE_HREF} from '@angular/common';
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    PublisherComponent,
    SubscriberComponent,
    VideoPublisherComponent,
    BtnVideoComponent,
    VideoSubscriberComponent,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    AngularDraggableModule,
    AppRoutingModule,
    HttpClientModule
  ],
    providers: [{
      provide: APP_BASE_HREF,
      useValue: environment.BASE_HREF
    }],
  bootstrap: [AppComponent],
  entryComponents: [
    VideoPublisherComponent,
    BtnVideoComponent
  ]
})

export class AppModule {
  constructor(private injector: Injector) {
  }
  ngDoBootstrap() {
    const myCustomElement = createCustomElement(VideoPublisherComponent, { injector: this.injector });
    const btnVideo = createCustomElement(BtnVideoComponent, { injector: this.injector });
    customElements.define('app-video-publisher', myCustomElement);
    customElements.define('app-btn-video', btnVideo);
  }
}
