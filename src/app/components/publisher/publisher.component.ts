import {Component, ElementRef, AfterViewInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import { OpentokService } from '../../services/opentok.service';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})

export class PublisherComponent implements AfterViewInit {
  @ViewChild('publisherDiv', {static: false}) publisherDiv: ElementRef;
  @Output() sendUrl = new EventEmitter<boolean>();
  @Output() closeCall = new EventEmitter<boolean>();
  @Input() session: OT.Session;

  publisher: OT.Publisher;
  publishing: Boolean;
  videoEnabled = false;
  audioEnabled = false;
  statusVideo = false;
  statusSound = false;

  constructor(private opentokService: OpentokService ){
      this.publishing = true;
  }

  ngAfterViewInit() {
    const OT = this.opentokService.getOT();
    this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, {
        fitMode: 'cover',
        insertMode: 'append',
        width: '100%',
        height: '100%',
        publishAudio: false,
        publishVideo: false
      });

    this.opentokService.publisher = this.publisher;

    if (this.session) {
      if (this.session['isConnected']()) {
        this.publish();
      }
      this.session.on('sessionConnected', () => this.publish());
    }
  }


  publish() {
    this.session.publish(this.publisher, (err) => {
      if (err) {
        alert(err.message);
      } else {
        this.publishing = true;
      }
    });
  }

  startStopVideo() {
    this.statusVideo = !this.statusVideo;
    if (this.videoEnabled) {
      this.opentokService.StopVideo();
      this.videoEnabled = false;
    } else {
      this.opentokService.startVideo();
      this.videoEnabled = true;
    }
  }

  muteAndUnMuteAudio() {
    this.statusSound = !this.statusSound;
    if (this.audioEnabled) {
      this.opentokService.muteAudio();
      this.audioEnabled = false;
    } else {
      this.opentokService.unMuteAudio();
      this.audioEnabled = true;
    }
  }

  leaveConvo() {
    this.opentokService.leaveConversation();
    this.opentokService.tipifica();
  }
}
