import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OpentokService} from "../../services/opentok.service";

@Component({
  selector: 'app-btn-video',
  templateUrl: './btn-video.component.html',
  styleUrls: ['./btn-video.component.css']
})
export class BtnVideoComponent implements OnInit {
  session: OT.Session;

  @Output() actionClosed: EventEmitter<any> = new EventEmitter<any>();
  constructor(private readonly opentokService: OpentokService) { }

  ngOnInit() {
  }

  abrirConexion(): void {
    this.opentokService.visualizevideoPublisher();
    this.actionClosed.emit(this.opentokService.urlRoom());
  }
}
