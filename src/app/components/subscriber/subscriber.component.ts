import {Component, ElementRef, AfterViewInit, ViewChild, Input, ChangeDetectorRef} from '@angular/core';
import { OpentokService } from '../../services/opentok.service';
import * as OT from '@opentok/client';

@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css']
})


export class SubscriberComponent implements AfterViewInit {
  @ViewChild('subscriberDiv', {static: true}) subscriberDiv: ElementRef;
  @Input() session: OT.Session;
  @Input() stream: OT.Stream;

  subscriber: OT.Publisher;
  videoEnabled = false;
  publishSub: Boolean;
  statusVideo = false;
  audioEnabled = false;
  statusSound = false;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;

  constructor(
    private opentokService: OpentokService){
    this.publishSub = true;
  }
  ngAfterViewInit() {
    const OT = this.opentokService.getOT();
    this.subscriber = OT.initPublisher(this.subscriberDiv.nativeElement, {
      fitMode: 'cover',
      insertMode: 'append',
      width: '100%',
      height: '100%',
      publishAudio: false,
      publishVideo: false
    });
    this.opentokService.publisher = this.subscriber;
  }

  startStopVideo() {
    this.statusVideo = !this.statusVideo;
    if (this.videoEnabled) {
      this.opentokService.StopVideo();
      this.videoEnabled = false;
    } else {
      this.opentokService.startVideo();
      this.videoEnabled = true;
    }
  }

  muteAndUnMuteAudio() {
    this.statusSound = !this.statusSound;
    if (this.audioEnabled) {
      this.opentokService.muteAudio();
      this.audioEnabled = false;
    } else {
      this.opentokService.unMuteAudio();
      this.audioEnabled = true;
    }
  }
}
