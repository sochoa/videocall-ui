export interface DataChatInterface {
    chatId: number;
    did: string;
    campaignId: number;
    closedChats: number;
}
