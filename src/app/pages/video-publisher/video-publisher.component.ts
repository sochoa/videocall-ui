import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { OpentokService } from '../../services/opentok.service';
import {Observable} from "rxjs";

declare const require: any;

@Component({
  selector: 'app-video-publisher',
  templateUrl: './video-publisher.component.html',
  styleUrls: ['./video-publisher.component.css']
})

export class VideoPublisherComponent implements OnInit {
  session: OT.Session;
  streams: Array<OT.Stream> = [];
  token: string;
  changeDetectorRef: ChangeDetectorRef;
  draggable = true;
  ngResizable = true;

  visible: Observable<boolean> = this.opentokService.visiblePublisherComponent$;
  visibleTipi: Observable<boolean> = this.opentokService.visibleTipificationComponent$;

  constructor(
    private ref: ChangeDetectorRef,
    private opentokService: OpentokService
  ) {
    this.changeDetectorRef = ref;
  }

  ngOnInit () {
    this.generarToken();
  }

  generarToken() {
    this.opentokService.getToken(20821528).subscribe((token) => {
      this.token = token;
      this.addSession();
    });
  }
  addSession(){

    this.opentokService.initSession(this.token).then((session: OT.Session) => {
      this.session = session;
      this.session.on('streamCreated', (event) => {
        this.streams.push(event.stream);
        this.changeDetectorRef.detectChanges();
      });
      this.session.on('streamDestroyed', (event) => {
        const idx = this.streams.indexOf(event.stream);
        if (idx > -1) {
          this.streams.splice(idx, 1);
          this.changeDetectorRef.detectChanges();
        }
      });
    })
      .then(() => this.opentokService.connect())
      .catch((err) => {
        console.error(err);
        alert('Unable to connect.');
      });
  }
}
