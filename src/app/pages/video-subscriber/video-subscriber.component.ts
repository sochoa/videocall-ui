import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {OpentokService} from "../../services/opentok.service";
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-video-subscriber',
  templateUrl: './video-subscriber.component.html',
  styleUrls: ['./video-subscriber.component.css']
})
export class VideoSubscriberComponent implements OnInit {


  session: OT.Session;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;
  draggable = true;
  ngResizable = true;
  modalShare = false;
  token: string;
  sessionId: string;
  openModal = {
    modalShare : false
  }

  constructor(
    private ref: ChangeDetectorRef,
    private opentokService: OpentokService,
  private rutaActiva: ActivatedRoute
  ) {
    this.changeDetectorRef = ref;
  }

  ngOnInit () {
    this.rutaActiva.paramMap.subscribe(param => { this.sessionId = param.get("id"); this.addSession()});
  }

  addSession(){
    this.opentokService.initSession(this.sessionId).then((session: OT.Session) => {

      this.session = session;
      this.session.on('streamCreated', (event) => {
        console.log(event)
        this.streams.push(event.stream);
        this.changeDetectorRef.detectChanges();
      });
      this.session.on('streamDestroyed', (event) => {
        const idx = this.streams.indexOf(event.stream);
        if (idx > -1) {
          this.streams.splice(idx, 1);
          this.changeDetectorRef.detectChanges();
        }
      });
    })
      .then(() => this.opentokService.connect())
      .catch((err) => {
        console.error(err);
        alert('Unable to connect. Make sure you have updated the config.ts file with your OpenTok details.');
      });
  }


}
