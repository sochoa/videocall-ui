import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSubscriberComponent } from './video-subscriber.component';

describe('VideoSubscriberComponent', () => {
  let component: VideoSubscriberComponent;
  let fixture: ComponentFixture<VideoSubscriberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoSubscriberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSubscriberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
