import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VideoSubscriberComponent } from '../pages/video-subscriber/video-subscriber.component';
import { VideoPublisherComponent } from '../../app/pages/video-publisher/video-publisher.component';


const routes: Routes = [
  { path: '', redirectTo: 'publisher', pathMatch: 'full'},
  { path: 'publisher', component: VideoPublisherComponent },
  { path: 'room/:id', component: VideoSubscriberComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

