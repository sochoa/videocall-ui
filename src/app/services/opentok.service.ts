import { tap,  map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { environment } from '../../environments/environment';
import * as OT from '@opentok/client';


@Injectable({
  providedIn: 'root'
})

export class OpentokService {
  session: OT.Session;
  publisher: OT.Publisher;
  token: string;
  visiblePublisherComponent: BehaviorSubject<boolean> = new BehaviorSubject(null);
  visibleTipificationComponent: BehaviorSubject<boolean> = new BehaviorSubject(null);
  meet= environment.URL_HOST_VIDEO;

  constructor(private cliente: HttpClient) { }

  getOT() {
    return OT;
  }

  getToken(idCreacion: number): Observable<string> {
    return this.cliente.get(environment.URL_BASE_VIDEOCALL+`/session/${idCreacion}`).pipe(
      tap(console.log),
      map((data: any) => data.sessionId)
    )
  }

  initSession(sessionId: string) {
    return this.cliente.get(environment.URL_BASE_VIDEOCALL+`/join/${sessionId}`).pipe(
      map((data: any) => {
        this.session = this.getOT().initSession(data.apiKey, data.sessionId);
        this.token = data.token;
        return this.session;
      })
    ).toPromise();
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.session.connect(this.token, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(this.session);
        }
      });
    });
  }

  StopVideo() {
    this.publisher.publishVideo(false);
  }

  startVideo() {
    this.publisher.publishVideo(true);
  }

  muteAudio() {
    this.publisher.publishAudio(false);
  }

  unMuteAudio() {
    this.publisher.publishAudio(true);
  }
  leaveConversation() {
    this.session.disconnect();
    this.visiblePublisherComponent.next(false);
    setTimeout(function afterTwoSeconds() {
      window.close();
    }, 4000);
  }
  tipifica(){
    this.visibleTipificationComponent.next(true);
  }
  visualizevideoPublisher() {
    this.visiblePublisherComponent.next(true);
    this.visibleTipificationComponent.next(false);
  }

  urlRoom(){
    return "Para unirte a la videollamada ingresa al siguiente link: "+this.meet+"/room/"+this.session.sessionId;
  }
  unvisualizevideoPublisher() {
    this.visiblePublisherComponent.next(false);
  }
  get visiblePublisherComponent$() {
    return this.visiblePublisherComponent.asObservable();
  }
  get visibleTipificationComponent$() {
    return this.visibleTipificationComponent.asObservable();
  }
}





